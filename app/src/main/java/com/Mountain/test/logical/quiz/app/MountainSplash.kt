package com.Mountain.test.logical.quiz.app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import com.Mountain.test.logical.quiz.app.databinding.ActivityMountainSplashBinding
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class MountainSplash : AppCompatActivity() {
    private lateinit var binding: ActivityMountainSplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMountainSplashBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setFullScreen()

        GlobalScope.launch {
            delay(2800)
            val intent = Intent(this@MountainSplash, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun setFullScreen() {
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }
}