package com.Mountain.test.logical.quiz.app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import com.Mountain.test.logical.quiz.app.databinding.ActivityMainBinding
import com.Mountain.test.logical.quiz.app.databinding.ActivityMountainSplashBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setFullScreen()


        setFullScreen()

        binding.startBtn.setOnClickListener {
            val intent = Intent(this@MainActivity, MontBlancActivity::class.java)
            startActivity(intent)
        }

        binding.exitBtn.setOnClickListener {
            finish()
        }


    }
    private fun setFullScreen() {
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }
}