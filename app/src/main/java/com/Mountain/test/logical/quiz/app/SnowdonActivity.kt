package com.Mountain.test.logical.quiz.app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.WindowManager
import com.Mountain.test.logical.quiz.app.databinding.ActivitySnowdonBinding

class SnowdonActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySnowdonBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySnowdonBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setFullScreen()

        var correctAnswer = intent.getIntExtra("score", 0)
        Log.d("FoxScreenScore", "$correctAnswer ")

        binding.option1.setOnClickListener {
            binding.option1.setBackgroundColor(resources.getColor(R.color.green))
            correctAnswer++
            binding.option2.isClickable = false
            binding.option3.isClickable = false
        }

        binding.option2.setOnClickListener {
            binding.option2.setBackgroundColor(resources.getColor(R.color.red))
            binding.option1.isClickable = false
            binding.option3.isClickable = false
        }

        binding.option3.setOnClickListener {
            binding.option3.setBackgroundColor(resources.getColor(R.color.red))
            binding.option2.isClickable = false
            binding.option1.isClickable = false
        }

        binding.nextBtn.setOnClickListener {
            val intent = Intent(this, CaucasusMountainsActivity::class.java)
            intent.putExtra("score", correctAnswer)
            startActivity(intent)
            finish()
        }
        binding.imageView.setOnClickListener {
            finish()
        }
    }

    private fun setFullScreen() {
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }
}